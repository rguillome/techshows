package fr.grenade.demo.multithreading.show.samples.sample2;

import fr.grenade.demo.multithreading.show.core.AbstractCompteur;

public class Sample2Compteur extends AbstractCompteur {

	private long value = 0;

	private final Object keyAdd = new Object();

	public long ajouter() {
		synchronized (keyAdd) {
			return ++value;
		}

	}

	@Override
	public long diminuer() {
		return --value;
	}

	@Override
	public long getCurrentValue() {
		return value;
	}
}