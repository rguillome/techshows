package fr.grenade.demo.multithreading.show.samples.sample4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.grenade.demo.multithreading.show.core.AbstractThread;

public class Sample4Thread extends AbstractThread<Sample4Compteur> {

	static final Logger LOG = LoggerFactory.getLogger(Sample4Thread.class);

	private Sample4Compteur compteur;

	public long maxLoop = 10000;

	@Override
	public void run() {

		long result = -1;

		for (int i = 0; i < maxLoop; i++) {

			if (i % 2 == 0) {
				result = compteur.ajouter();
			} else {
				result = compteur.diminuer();
			}

			LOG.info("" + Thread.currentThread().getName() + ", Compteur : " + result);
		}

	}

	@Override
	public void setCompteur(Sample4Compteur compteur) {
		this.compteur = compteur;

	}

	@Override
	public void setMaxLoop(long maxLoop) {
		this.maxLoop = maxLoop;

	}

}
