package fr.grenade.demo.multithreading.show.core;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LaunchSample {

	private static final transient Logger LOG = LoggerFactory.getLogger(LaunchSample.class);

	public <C extends AbstractCompteur, T extends AbstractThread<C>, T2 extends AbstractThread<C>> void runThreads(
			Class<T> typeOfThread1, Class<T2> typeOfThread2, C compteur, long maxLoop) throws InterruptedException,
			InstantiationException, IllegalAccessException {
		long begin = Calendar.getInstance().getTimeInMillis();

		T run1 = typeOfThread1.newInstance();
		run1.setCompteur(compteur);
		run1.setMaxLoop(maxLoop);
		
		T2 run2 = typeOfThread2.newInstance();
		run2.setCompteur(compteur);
		run2.setMaxLoop(maxLoop);
		
		Thread thread1 = new Thread(run1);
		Thread thread2 = new Thread(run2);

		thread1.start();
		thread2.start();

		thread1.join();
		thread2.join();

		long end = Calendar.getInstance().getTimeInMillis();
		long duration = end - begin;

		LOG.info("Execution take : " + duration+ " ms");
	}
}
