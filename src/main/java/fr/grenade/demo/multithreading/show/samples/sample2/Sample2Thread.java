package fr.grenade.demo.multithreading.show.samples.sample2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.grenade.demo.multithreading.show.core.AbstractThread;

public class Sample2Thread extends AbstractThread<Sample2Compteur> {

	static final Logger LOG = LoggerFactory.getLogger(Sample2Thread.class);

	private Sample2Compteur compteur;

	public long maxLoop = 10000;

	@Override
	public void run() {

		for (int i = 0; i < maxLoop; i++) {

			long result = compteur.ajouter();

			LOG.info("" + Thread.currentThread().getName() + ", Compteur : " + result);
		}

	}

	@Override
	public void setCompteur(Sample2Compteur compteur) {
		this.compteur = compteur;

	}

	@Override
	public void setMaxLoop(long maxLoop) {
		this.maxLoop = maxLoop;

	}

}
