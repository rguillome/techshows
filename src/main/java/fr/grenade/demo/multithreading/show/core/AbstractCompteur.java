package fr.grenade.demo.multithreading.show.core;

public abstract class AbstractCompteur {

	public abstract long ajouter();

	public abstract long diminuer();

	public abstract long getCurrentValue();

}
