package fr.grenade.demo.multithreading.show.samples.sample5;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.grenade.demo.multithreading.show.core.AbstractThread;

public class Sample5Thread extends AbstractThread<Sample5Compteur> {

	static final Logger LOG = LoggerFactory.getLogger(Sample5Thread.class);

	private Sample5Compteur compteur;

	public long maxLoop = 10000;

	@Override
	public void run() {

		long result = -1;

		for (int i = 0; i < maxLoop; i++) {

			if (i % 2 == 0) {
				result = compteur.ajouter();
			} else {
				result = compteur.diminuer();
			}

			LOG.info("" + Thread.currentThread().getName() + ", Compteur : " + result);
		}

	}

	@Override
	public void setCompteur(Sample5Compteur compteur) {
		this.compteur = compteur;

	}

	@Override
	public void setMaxLoop(long maxLoop) {
		this.maxLoop = maxLoop;

	}

}
