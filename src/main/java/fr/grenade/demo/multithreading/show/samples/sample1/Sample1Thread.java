package fr.grenade.demo.multithreading.show.samples.sample1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.grenade.demo.multithreading.show.core.AbstractThread;

public class Sample1Thread extends AbstractThread<Sample1Compteur> {

	static final Logger LOG = LoggerFactory.getLogger(Sample1Thread.class);

	private Sample1Compteur compteur;

	public long maxLoop = 100000;

	@Override
	public void run() {

		for (int i = 0; i < maxLoop; i++) {

			long result = compteur.ajouter();

			LOG.info("" + Thread.currentThread().getName() + ", Compteur : " + result);
		}

	}

	@Override
	public void setCompteur(Sample1Compteur compteur) {
		this.compteur = compteur;

	}

	@Override
	public void setMaxLoop(long maxLoop) {
		this.maxLoop = maxLoop;

	}

}
