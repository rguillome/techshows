package fr.grenade.demo.multithreading.show.core;

public abstract class AbstractThread<C extends AbstractCompteur> extends Thread {
	
	public abstract void setCompteur(C compteur);
	
	public abstract void setMaxLoop(long maxLoop);

}
