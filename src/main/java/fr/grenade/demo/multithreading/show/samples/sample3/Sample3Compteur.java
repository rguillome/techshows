package fr.grenade.demo.multithreading.show.samples.sample3;

import fr.grenade.demo.multithreading.show.core.AbstractCompteur;

public class Sample3Compteur extends AbstractCompteur {

	private long value = 0;

	private final Object keyAdd = new Object();
	private final Object keySub = new Object();

	public long ajouter() {
		synchronized (keyAdd) {
			return ++value;
		}

	}

	@Override
	public long diminuer() {
		synchronized (keySub) {
			return --value;
		}
	}

	@Override
	public long getCurrentValue() {
		return value;
	}
}