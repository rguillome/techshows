package fr.grenade.demo.multithreading.show.samples.sample5;

import fr.grenade.demo.multithreading.show.core.AbstractCompteur;

public class Sample5Compteur extends AbstractCompteur {

	private long value = 0;

	private final Object key1 = new Object();
	private final Object key2 = new Object();

	public long ajouter() {
		synchronized (key1) {
			synchronized (key2) {
				return ++value;
			}
		}

	}

	@Override
	public long diminuer() {
		synchronized (key2) {
			synchronized (key1) {
				return --value;
			}
		}
	}

	@Override
	public long getCurrentValue() {
		return value;
	}
}