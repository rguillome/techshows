package fr.grenade.demo.multithreading.show.samples.sample1;

import fr.grenade.demo.multithreading.show.core.AbstractCompteur;

public class Sample1Compteur extends AbstractCompteur {

	private long value = 0;

	public long ajouter() {
		return ++value;

	}

	@Override
	public long diminuer() {
		return --value;
	}

	@Override
	public long getCurrentValue() {
		return value;
	}
}