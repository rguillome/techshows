package fr.grenade.demo.multithreading.show.samples.sample4;

import fr.grenade.demo.multithreading.show.core.AbstractCompteur;

public class Sample4Compteur extends AbstractCompteur {

	private long value = 0;

	private final Object keyAdd = new Object();

	public long ajouter() {
		synchronized (keyAdd) {
			return ++value;
		}

	}

	@Override
	public long diminuer() {
		synchronized (keyAdd) {
			return --value;
		}
	}

	@Override
	public long getCurrentValue() {
		return value;
	}
}