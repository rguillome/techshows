package fr.grenade.demo.multithreading.show.samples.sample3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.grenade.demo.multithreading.show.core.AbstractThread;

public class Sample3Thread extends AbstractThread<Sample3Compteur> {

	static final Logger LOG = LoggerFactory.getLogger(Sample3Thread.class);

	private Sample3Compteur compteur;

	public long maxLoop = 10000;

	@Override
	public void run() {

		long result = -1;

		for (int i = 0; i < maxLoop; i++) {

			if (i % 2 == 0) {
				result = compteur.ajouter();
			} else {
				result = compteur.diminuer();
			}

			LOG.info("" + Thread.currentThread().getName() + ", Compteur : " + result);
		}

	}

	@Override
	public void setCompteur(Sample3Compteur compteur) {
		this.compteur = compteur;

	}

	@Override
	public void setMaxLoop(long maxLoop) {
		this.maxLoop = maxLoop;

	}

}
