package fr.grenade.demo.multithreading.show;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

import fr.grenade.demo.multithreading.show.core.LaunchSample;
import fr.grenade.demo.multithreading.show.samples.sample1.Sample1Compteur;
import fr.grenade.demo.multithreading.show.samples.sample1.Sample1Thread;
import fr.grenade.demo.multithreading.show.samples.sample2.Sample2Compteur;
import fr.grenade.demo.multithreading.show.samples.sample2.Sample2Thread;
import fr.grenade.demo.multithreading.show.samples.sample3.Sample3Compteur;
import fr.grenade.demo.multithreading.show.samples.sample3.Sample3Thread;
import fr.grenade.demo.multithreading.show.samples.sample4.Sample4Compteur;
import fr.grenade.demo.multithreading.show.samples.sample4.Sample4Thread;
import fr.grenade.demo.multithreading.show.samples.sample5.Sample5Compteur;
import fr.grenade.demo.multithreading.show.samples.sample5.Sample5Thread;

public class ShowLauncherTest {

	public static final long MAX_LOOP = 10000000L;

	@Test
	public void shouldLaunchSample1() throws InterruptedException, InstantiationException, IllegalAccessException {

		LaunchSample launcher = new LaunchSample();

		Sample1Compteur compteur = new Sample1Compteur();

		launcher.runThreads(Sample1Thread.class, Sample1Thread.class, compteur, MAX_LOOP);

		assertThat(compteur.getCurrentValue()).isEqualTo(MAX_LOOP * 2L);

	}

	@Test
	public void shouldLaunchSample2() throws InterruptedException, InstantiationException, IllegalAccessException {

		LaunchSample launcher = new LaunchSample();

		Sample2Compteur compteur = new Sample2Compteur();

		launcher.runThreads(Sample2Thread.class, Sample2Thread.class, compteur, MAX_LOOP);

		assertThat(compteur.getCurrentValue()).isEqualTo(MAX_LOOP * 2L);

	}

	@Test
	public void shouldLaunchSample3() throws InterruptedException, InstantiationException, IllegalAccessException {

		LaunchSample launcher = new LaunchSample();

		Sample3Compteur compteur = new Sample3Compteur();

		launcher.runThreads(Sample3Thread.class, Sample3Thread.class, compteur, MAX_LOOP);

		assertThat(compteur.getCurrentValue()).isEqualTo(0);

	}

	@Test
	public void shouldLaunchSample4() throws InterruptedException, InstantiationException, IllegalAccessException {

		LaunchSample launcher = new LaunchSample();

		Sample4Compteur compteur = new Sample4Compteur();

		launcher.runThreads(Sample4Thread.class, Sample4Thread.class, compteur, MAX_LOOP);

		assertThat(compteur.getCurrentValue()).isEqualTo(0);

	}

	@Test
	public void shouldLaunchSample5() throws InterruptedException, InstantiationException, IllegalAccessException {

		LaunchSample launcher = new LaunchSample();

		Sample5Compteur compteur = new Sample5Compteur();

		launcher.runThreads(Sample5Thread.class, Sample5Thread.class, compteur, MAX_LOOP);

		assertThat(compteur.getCurrentValue()).isEqualTo(0);

	}
}
